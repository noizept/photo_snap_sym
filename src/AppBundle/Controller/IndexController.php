<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");

class IndexController extends Controller
{

    /**
     * @Route("/index")
     */
    public function indexAction()
    {

        return $this->render(
            'uploadIndex/upload.html.twig');
    }

    /**
     * @Route("/index/upload")
     */
    public function indexUpload(Request $request){

        // Gets Services
        $s3_client=$this->get('aws_s3');
        $photoUtilities=$this->get('photo_uti');

        // Connects to S3
        $s3_client->setCredentials($this->getParameter('aws_id'),$this->getParameter('aws_secret'),$this->getParameter('aws_bucket'));

        //Uploads file to S3
        $url=$s3_client->uploadtoaws($request->files->get('file'),'TestingUps');

        //array longitude , latitude, timetaken
        $photoDetails=$photoUtilities->photo_info($s3_client->getTempPath());
        $s3_client->unlink();

        //Create photo object
        $photo=new Photo();
        $photo->setLongitude($photoDetails['longitude']);
        $photo->setLatitude($photoDetails['latitude']);
        $photo->setLink($url);
        $photo->setPhotoFilter('no filter');
        $photo->setTimeTaken($photoDetails['timetaken']);
        $photo->setPhotographer('empty');
        $photo->setMessage('empty');
        $photo->setStatusCode(0);



        $em=$this->getDoctrine()->getManager();
        $em->persist($photo);
        $em->flush();
        return new JsonResponse([]);

    }



}
