<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhotoHandleController extends Controller
{

    /**
     * @Route("/photos")
     */
    public function indexAction()
    {
        $em=$this->getDoctrine()->getManager();
        $photos=$em->createQuery('SELECT p
                                  FROM AppBundle:Photo p
                                  WHERE p.status_code = :status_code')
                    ->setParameter('status_code',0)
                    ->setMaxResults(6)
                    ->getResult();
        $photographer=$em->createQuery(' Select p
                                        FROM AppBundle:Photographer p
                                        ')->getResult();

        return $this->render('photopage/photos.html.twig',array(
            'MyFotos'=>$photos,'Photographers'=>$photographer));
    }

    /**
     * @Route("/photoupdate")
     */
    public function photoUpdate(Request $request){
        $my_post=$request->request;
        $aws=$this->get('aws_s3');
        $photo_util=$this->get('photo_uti');
        $aws->setCredentials($this->getParameter('aws_id'),
                            $this->getParameter('aws_secret'),
                             $this->getParameter('aws_bucket'));
        (!strpos($my_post->get('newPhoto'),'feather-client-files'))?$url="no filter":
            $url=$aws->uploadFilteredPhoto('TestingUps',$my_post->get('newPhoto'));



        empty($my_post->get('photoTime'))?$time=new \DateTime('now'):$time=new \DateTime($photo_util->timepicker_to_dbtime($my_post->get('photoTime')));
        empty($my_post->get('Latitude'))?$latitude=0:$latitude=$my_post->get('Latitude');
        empty($my_post->get('Longitude'))?$longitude=0:$longitude=$my_post->get('Longitude');
        empty($my_post->get('message'))?$message='empty':$message=$my_post->get('message');

        $repository = $this->getDoctrine()->getManager();

        $photo =$repository->getRepository('AppBundle:Photo')->find($my_post->get('photoID'));

        $photo->setPhotographer($my_post->get('photographer'));
        $photo->setLatitude($latitude);
        $photo->setLongitude($longitude);
        $photo->setStatusCode($my_post->get('aceptance'));
        $photo->setPhotoFilter($url);
        $photo->setTimeTaken($time);
        $photo->setMessage($message);
        $repository->flush();
        return new JsonResponse([]);
    }

}
