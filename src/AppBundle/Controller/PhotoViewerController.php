<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class PhotoViewerController extends Controller
{
    /**
     * @Route("/image/{photoid}")
     */
    public function PhotoAction($photoid)
    {
        $repository = $this->getDoctrine()->getManager();
        $photo =$repository->getRepository('AppBundle:Photo')->find($photoid);
        return $this->render('photoView/photoViewer.html.twig',array('photo'=>$photo));
    }
}
