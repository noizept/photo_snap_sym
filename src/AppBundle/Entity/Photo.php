<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 04-08-2015
 * Time: 5:55
 */


/**
 * @ORM\Entity
 * @ORM\Table(name="photo")
 */
class Photo
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(type="string", length=100) */
    protected $photographer;


    /** @ORM\Column(type="decimal",  precision=22, scale=6) */
    protected $latitude;

    /** @ORM\Column(type="decimal" , precision=22, scale=6) */
    protected $longitude;

    /** @ORM\Column(type="text") */
    protected $message;

    /** @ORM\Column(type="string", length=100) */
    protected $link;

    /** @ORM\Column(type="datetime") */
    protected $time_taken;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status_code;
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $photo_filter;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Photo
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Photo
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Photo
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set time_taken
     *
     * @param \DateTime $timeTaken
     * @return Photo
     */
    public function setTimeTaken($timeTaken)
    {
        $this->time_taken = $timeTaken;

        return $this;
    }

    /**
     * Get time_taken
     *
     * @return \DateTime 
     */
    public function getTimeTaken()
    {
        return $this->time_taken;
    }

    /**
     * Set photo_filter
     *
     * @param string $photoFilter
     * @return Photo
     */
    public function setPhotoFilter($photoFilter)
    {
        $this->photo_filter = $photoFilter;

        return $this;
    }

    /**
     * Get photo_filter
     *
     * @return string 
     */
    public function getPhotoFilter()
    {
        return $this->photo_filter;
    }

    /**
     * Set status_code
     *
     * @param integer $statusCode
     * @return Photo
     */
    public function setStatusCode($statusCode)
    {
        $this->status_code = $statusCode;

        return $this;
    }

    /**
     * Get status_code
     *
     * @return integer 
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * Set photographer
     *
     * @param string $photographer
     * @return Photo
     */
    public function setPhotographer($photographer)
    {
        $this->photographer = $photographer;

        return $this;
    }

    /**
     * Get photographer
     *
     * @return string 
     */
    public function getPhotographer()
    {
        return $this->photographer;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Photo
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
}
