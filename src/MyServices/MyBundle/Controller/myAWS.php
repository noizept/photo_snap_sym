<?php

namespace MyServices\MyBundle\Controller;

use Aws\S3\S3Client;

class myAWS
{
    private  $mS3Bucket;
    private $mClient;
    private $filepath;

    public function setCredentials($aAWS_ID,$aAWS_SECRET,$aS3Bucket){
        $this->mS3Bucket=$aS3Bucket;
        $this->mClient=S3Client::factory([
            'region'=>'us-west-2',
            'credentials' => array(
                'key'    => $aAWS_ID,
                'secret' => $aAWS_SECRET,
            ),
            'version'=>'latest'

        ]);
    }

    public  function uploadtoaws($FILE,$user_email){
        $key=md5(uniqid());
        $tmp_file_name= "{$key}.{$FILE->guessExtension()}";
        $tmp_file_path=$_SERVER['DOCUMENT_ROOT']."/uploads/";

        //move file
        $FILE->move($tmp_file_path,$tmp_file_name);
        return $this->__upload($user_email,$tmp_file_name,$tmp_file_path);

    }
    private function __upload($aUser_email,$aName,$aTmp_file_path){
        $upload= $this->mClient->upload($this->mS3Bucket,"{$aUser_email}/{$aName}",fopen($aTmp_file_path.'/'.$aName,'rb'),'public-read');
        $this->setTempPath($aTmp_file_path.'/'.$aName);
        return $upload->get('ObjectURL');
    }

    private function setTempPath($aFilePath){
        $this->filepath=$aFilePath;
    }
    public function getTempPath(){
        return $this->filepath;
    }
    public function unlink(){
        unlink($this->filepath);

    }
    public function uploadFilteredPhoto($folder,$file){
        $photoname=explode('/',$file);
        $photoname=strtolower(end($photoname));
        $photoname='_filter_'.$photoname;

        $upload = $this->mClient->upload($this->mS3Bucket,"{$folder}/{$photoname}",fopen($file,'rb'),'public-read');
        return $upload->get('ObjectURL');
    }


}