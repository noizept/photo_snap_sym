<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 07-08-2015
 * Time: 4:32
 */

namespace MyServices\MyBundle\Controller;


use Exception;

class photo_utilities
{

    public function photo_info($aFilePath){

        $exif=exif_read_data($aFilePath);
       try{
            $latitude = $this->gps($exif["GPSLatitude"], $exif['GPSLatitudeRef']);
               $longitude = $this->gps($exif["GPSLongitude"], $exif['GPSLongitudeRef']);
               $timetaken = new \DateTime($this->time_conv($exif));
           }
           catch(Exception $e){
            $latitude = 0;
            $longitude = 0;
            $timetaken = new \DateTime('now');
        }
        return array('latitude'=>$latitude,'longitude'=>$longitude,'timetaken'=>$timetaken);

    }
    private function gps($coordinate, $hemisphere) {
        for ($i = 0; $i < 3; $i++) {
            $part = explode('/', $coordinate[$i]);
            if (count($part) == 1) {
                $coordinate[$i] = $part[0];
            } else if (count($part) == 2) {
                $coordinate[$i] = floatval($part[0])/floatval($part[1]);
            } else {
                $coordinate[$i] = 0;
            }
        }
        list($degrees, $minutes, $seconds) = $coordinate;
        $sign = ($hemisphere == 'W' || $hemisphere == 'S') ? -1 : 1;
        return $sign * ($degrees + $minutes/60 + $seconds/3600);
    }
    private function time_conv($exif){
        $extension= explode(' ',$exif['DateTime']);
        $extension[0]=str_replace(':','-',$extension[0]);
        $extension=join(' ',$extension);
        return $extension;
    }
    function timepicker_to_dbtime($aTime){
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/", $aTime)){
            return $aTime;
        }
        $time=explode(' ',$aTime);
        $time[0] =str_replace('/','-',$time[0]);
        $time=join(' ',$time);
        $time.=':00';
        return $time;
    }

}