<?php

namespace MyServices\MyBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        die($this->get('myAWSer')->greet($name));
        //return $this->render('MyServicesMyBundle:Default:index.html.twig', array('name' => $name));
    }
}

