<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Ensure i can see the photo with id 41');
$I->amOnPage('image/41');
$I->see('<img src="https://s3-us-west-2.amazonaws.com/synfony/TestingUps/5a8214a84e098279326f818c23283bc2.jpeg"');
